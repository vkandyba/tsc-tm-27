package ru.vkandyba.tm.exception.entity;

import ru.vkandyba.tm.exception.AbstractException;

public class ProjectNotFoundException extends AbstractException {

    public ProjectNotFoundException() {
        super("Error! Project not found!");
    }
}
