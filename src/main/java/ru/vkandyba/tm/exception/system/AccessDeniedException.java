package ru.vkandyba.tm.exception.system;

import ru.vkandyba.tm.exception.AbstractException;

public class AccessDeniedException extends AbstractException {

    public AccessDeniedException() {
        super("Error! Accec denied!");
    }
}
