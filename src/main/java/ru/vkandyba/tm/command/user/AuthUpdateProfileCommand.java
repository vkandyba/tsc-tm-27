package ru.vkandyba.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.command.AbstractCommand;
import ru.vkandyba.tm.constant.TerminalConst;
import ru.vkandyba.tm.enumerated.Role;
import ru.vkandyba.tm.exception.system.AccessDeniedException;
import ru.vkandyba.tm.util.TerminalUtil;

public class AuthUpdateProfileCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "update-profile";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Update profile...";
    }

    @Override
    public void execute() {
        if (!serviceLocator.getAuthService().isAuth()) {
            throw new AccessDeniedException();
        }
        System.out.println("Enter First Name");
        @Nullable final String firstName = TerminalUtil.nextLine();
        System.out.println("Enter Last Name");
        @Nullable final String lastName = TerminalUtil.nextLine();
        System.out.println("Enter Middle Name");
        @Nullable final String middleName = TerminalUtil.nextLine();
        serviceLocator.getUserService().updateUser(
                serviceLocator.getAuthService().getUserId(),
                firstName, lastName, middleName
        );
    }

    @NotNull
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
